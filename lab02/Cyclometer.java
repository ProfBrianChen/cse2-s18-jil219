// CSE2 LAB02 JIAHUI LI

public class Cyclometer {
  // Prints data of the front wheel motions 
  public static void main(String[] args){
    // Input data
		int secsTrip1=480;  // Seconds for Trip1
		int secsTrip2=3220;  // Seconds for Trip2
		int countsTrip1=1561;  // Rotations in Trip1
		int countsTrip2=9037; // Rotations in Trip2
    
    // Intermediate variables and output data
		double wheelDiameter=27.0; // Assign variable to diameter of the wheel
		double PI=3.14159; // For calculation involving circles
		double feetPerMile=5280; // For conversion between feet and mile
		double inchesPerFoot=12; // For conversion between inches and foot
		double secondsPerMinute=60; // For conversion between seconds and minute
		double distanceTrip1, distanceTrip2, totalDistance;  // Declare variables 
    
    System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
    
    // Run the calculations, store the values 
    distanceTrip1=(double)(countsTrip1*wheelDiameter*PI);
    // Above gives distance in inches
    // (for each count, a rotation of the wheel travels
    //the diameter in inches times PI)
    distanceTrip1/=inchesPerFoot*feetPerMile; // Convert distance in Trip1 into miles
    distanceTrip2=(double)(countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile); // Calculates distance of Trip2 in miles 
    totalDistance=distanceTrip1+distanceTrip2; // Calculates total distance of the two trips  
    
    // Prints out the output data
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");

    
  } // End of main method 
  
} // End of class 