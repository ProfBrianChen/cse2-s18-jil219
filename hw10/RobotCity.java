///////////////////////////////
//CSE2 HW10 JIAHUI LI
//Generate random matrix representing city blocks
//add robots invasion and update 5 times
///////////////////////////////

import java.util.Random;

public class RobotCity{
  public static void main(String[] args){//main method
    Random rand = new Random();
    System.out.println("The original city matrix: ");
    int[][] cityArray = buildCity();//call method to generate matrix
    display(cityArray);//call method to print matrix
    int h = cityArray.length;//number of rows of the matrix
    int w = cityArray[0].length;//number of columns of the matrix
    int k = rand.nextInt(h*w + 1);//maximum of number of robots 
    System.out.println("There are "+ k +" robots.");
    System.out.println("City after robots invasion: ");
    cityArray = invade(cityArray,k);//call method to generate matrix after invasion 
    display(cityArray);//print array after invasion 
    for(int i=1; i<6; i++){//update and display in a loop 5 times 
      System.out.println("City matrix after "+ i +" update.");
      cityArray = update(cityArray);//call method to update 
      display(cityArray);
    }
  }
  public static int[][] buildCity(){//method to generate city array 
    Random rand = new Random();
    int w = rand.nextInt(6)+10;//assign random width
    int h = rand.nextInt(6)+10;//assign random height
    //System.out.println("\nThe width is "+ w +" and the height is "+ h +".");
    int[][] array = new int[w][h];//array will be saved in column-major form 
      for(int j=0; j<w; j++){//array for each column
        array[j] = new int[h];
        for(int i=0; i<h; i++){//assign column values 
          array[j][i] = rand.nextInt(900)+100;//assign random integer to each block 
        }
    }
    return array;
  }
  public static void display(int[][] array){//method to display array 
    int h = array.length;//get array height 
    int w = array[0].length;//get array width
    //System.out.println("The 0-th member of the 0-th column is "+ array[0][0] +".");
    for(int j=w-1; j>=0; j--){//column matrix 
      System.out.printf("%1s","[");
      for(int i=0; i<h; i++){//row number 
        System.out.printf("%5d",array[i][j]);//print elements 
      }
      System.out.printf("%2s","]");
      System.out.println();//start new line
      }
  }
  public static int[][] invade(int[][] array,int k){//method to generate robots invasion 
    Random rand = new Random();
    int i = 0;//index for assigning robots 
    do{//spot random location on the array 
      int h = rand.nextInt(array.length);
      int w = rand.nextInt(array[0].length);
      if(array[h][w] > 0){//check if it's invaded 
        int temp = -1 * array[h][w];
        array[h][w] = temp;//invade it if it's not already invaded
        i++;//increment index after each invasion 
      }
    }while(i<k);//there will be k invaded blocks 
    return array; 
  }
  public static int[][] update(int[][] array){//method to update
    int h = array.length;//get array height
    int w = array[0].length;//get array width
    for(int j=w-1; j>=0; j--){
      for(int i=h-1; i>=0; i--){//start with the eastermost column
        if(array[i][j] < 0){//if block is invaded 
          int temp = -1 * array[i][j];
          array[i][j] = temp;//change it back to positive 
          if(i+1 < h){//move the robot one block east 
            int temp00 = -1 * Math.abs(array[i+1][j]);
            array[i+1][j] = temp00;//change it to negative 
          }}
      }}
    return array;
  }
  
}//end of class 