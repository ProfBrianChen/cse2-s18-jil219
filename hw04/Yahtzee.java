/*
CSE2 HW4 JIAHUI LI
Play Yahtzee and output
upper section initial, upper
section total with bonus, lower
section initial, and grand total
*/
import java.util.Scanner;

public class Yahtzee{
  
  public static void main(String[] args){
    
    Scanner scan = new Scanner(System.in);
    //asks user to choose between typing in and generating random integers
    System.out.print("Please select:" + "\n"
                    + "1. Type in the 5 rolls" + "\n"
                    + "2. Computer generates 5 random rolls" + "\n");
    //accept user's input
    int option = scan.nextInt();
    int ones = 0; //number of roll 1
    int twos = 0; //number of roll 2
    int threes = 0; //number of roll 3
    int fours = 0; //number of roll 4
    int fives = 0; //number of roll 5
    int sixes = 0; //number of roll 6
    
    //different actions for two different user selections
    if(option == 1){
      //asks user to input numbers wanted 
      System.out.print("Please enter the 5 numbers you want: \n");
      //accept user's input
      int user = scan.nextInt();
      //converts the integer from user to string
      String rolls = Integer.toString(user);
      //counts the length of the string 
      int l = rolls.length();
          if(l == 5){
            for(int i = 0; i < 5; i++){
              //read the numbers as char on each place of the string
              char num = rolls.charAt(i);
              //switch on 6 conditions 
              switch(num){
                case '1': ones += 1; break; //if 1 is generated, add 1 to counts of 1
                case '2': twos += 1; break; //if 2 is generated, add 1 to counts of 2
                case '3': threes += 1; break; //if 3 is generated, add 1 to counts of 3
                case '4': fours += 1; break; //if 4 is generated, add 1 to counts of 4
                case '5': fives += 1; break; //if 5 is generated, add 1 to counts of 5
                case '6': sixes += 1; break; //if 6 is generated, add 1 to counts of 6
                default: 
                  System.out.println("Number provided is in error"); 
                  System.exit(0); //exit program if input is invalid
                  break;
              }
            }
          }else{
            System.out.println("Number provided is in error");
            System.exit(0); //exit program if input is invalid
          }
    }else if(option == 2){
        for(int i = 0; i < 5; i++){
          //generates random integers from 1 to 6
          int roll = (int)((Math.random() * 6) + 1);
          switch(roll){
            case 1: ones += 1; break; //if 1 is generated, add 1 to counts of 1
            case 2: twos += 1; break; //if 2 is generated, add 1 to counts of 2
            case 3: threes += 1; break; //if 3 is generated, add 1 to counts of 3
            case 4: fours += 1; break; //if 4 is generated, add 1 to counts of 4
            case 5: fives += 1; break; //if 5 is generated, add 1 to counts of 5
            case 6: sixes += 1; break; //if 6 is generated, add 1 to counts of 6
          }
        }
    }else{
      System.out.println("Invalid input");
      System.exit(0); //exit program if input is invalid
    }

    //calculates upper section initial total
    int upperI = (1 * ones) + (2 * twos) + (3 * threes) + (4 * fours) + (5 * fives) + (6 * sixes);
    int upperT; //initializes upper section total with bonus 
    if(upperI >= 63){
      upperT = upperI + 35; //bonus if upper section initial greater than 63
    }else{
      upperT = upperI; //no bonus added if upper section initial les than 63
    }
    
    int threeK = 0; //initializes score for three of a kind
    int fourK = 0; //initializes score for four of a kind
    int straights = 0; //initializes score for straights 
    int fullH = 0; //initializes score for full house
    int yah = 0; //initializes score for yahtzee
    //initializes score for chance which is the total of all dice for any instance 
    int chance = (1 * ones) + (2 * twos) + (3 * threes) + (4 * fours) + (5 * fives) + (6 * sixes);
    
    //a number occurs more than three times
    if(ones >= 3 || twos >= 3 || threes >= 3 || fours >= 3 || fives >= 3 || sixes >= 3){
      if(ones == 4 || twos == 4 || threes == 4 || fours == 4 || fives == 4 || sixes == 4){
        fourK += chance; //4 of a kind has same score as chance 
        threeK += chance; //4 of a kind situation also scores 3 of a kind
      }else if(ones == 5 || twos == 5 || threes == 5 || fours == 5 || fives == 5 || sixes == 5){
        yah += 50; //Yahtzee has score of 50
        fourK += chance; //Yahtzee situation also scores 4 of a kind
        threeK += chance; //Yahtzee situation also scores 3 of a kind
      }else if(ones == 2 || twos == 2 || threes == 2 || fours == 2 || fives == 2 || sixes == 2){
        fullH += 25; //a full house has score of 25
        threeK += chance; //full house also scores 3 of a kind
      }else{
        threeK += chance; //3 of a kind has same score as chance 
      }
    //straight with 1-2-3-4 and another number 
    }else if(ones >= 1 && twos >= 1 && threes >= 1 && fours >= 1){
      if(fives >= 1){
        //this is a large straight, 1-2-3-4-5,
        //which scores for both small straight (30) and large straight (40)
        straights += 70;
      }else{
        //small straight if no 5
        straights += 30;
      }
    //straight with 2-3-4-5 and another number 
    }else if(twos >= 1 && threes >= 1 && fours >= 1 && fives >= 1){
      if(ones >= 1 || sixes >= 1){
        //with one 1 or one 6,
        //there is a large straight 1-2-3-4-5 or 2-3-4-5-6
        straights += 70;
      }else{
        //2-3-4-5 makes it a small straight 
        straights += 30;
      }
    //straight with 3-4-5-6 and another number 
    }else if(threes >= 1 && fours >= 1 && fives >= 1 && sixes >= 1){
      if(twos >= 1){
        //makes it a large straight of 2-3-4-5-6
        straights += 70;
      }else{
        //otherwise its a small straight 
        straights += 30;
      }
    }else{
      //no other special case,
      //chance equals itself 
      chance += 0;
    }
    
    //calculates lower section initial
    int lowerI = threeK + fourK + straights + fullH + yah + chance;
    //calculates the grand total 
    int grandT = upperT + lowerI;
    
    //print out result 
    System.out.println("Upper section initial total is: " + upperI
                      + "\nUpper section total with bonus is: " + upperT
                      + "\nLower section total is: " + lowerI
                      + "\nThe grand total is: " + grandT);
  }
  
}