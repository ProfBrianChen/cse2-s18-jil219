/////////////////////////////////////////////////
//CSE2 HW08-Linear JIAHUI LI
//prompt user for array of 15 ints 
//do binary search, shuffle, and linear search 
////////////////////////////////////////////////////

import java.util.Scanner;
import java.util.Random; 
import java.util.Arrays;

public class CSE2Linear{//beginning of class
  public static void main (String[] args){//beginning of main method 
    Scanner myScanner = new Scanner(System.in);//declare scanner object 
    
    int[] stG = new int[15];//initialize array of 15 ints 
      System.out.print("Enter 15 ascending ints for final grades in CSE2: ");
      for(int i=0; i<stG.length; i++){
        if(!myScanner.hasNextInt()){//when array contains not int 
          System.out.println("Error: Only integers are acceptable.\n");
          System.exit(0);//end program 
        }
        stG[i] = myScanner.nextInt();//store user input in the array
        if(stG[i] < 0 || stG[i] > 100){//when int out of range 
          System.out.println("Error: Integers need to be between 0 and 100.\n");
          System.exit(0);//end program 
        }
        if(i>0){
          if(stG[i] < stG[i-1]){//when not in ascending order 
            System.out.println("Error: Integers need to be ascending.\n");
            System.exit(0);//end program 
          }
        }
    }
    System.out.println("You entered: " + Arrays.toString(stG));
    System.out.print("Please enter a grade to be searched for: ");//prompt user for int to search 
    int uSearch1 = myScanner.nextInt();//get user input 
    BiSearch(stG,uSearch1);//call method for binary search
    stG = Shuffle(stG);//call method for shuffling ints and replace the list with shuffled list 
    System.out.println("Scrambled: " + Arrays.toString(stG));
    System.out.print("Please enter a grade to be searched for: ");//prompt user for int to search 
    int uSearch2 = myScanner.nextInt();//get user input 
    LiSearch(stG,uSearch2);//call method for linear search 
  }
  public static void BiSearch(int[] grades,int s){
    int lowIn = 0;//initialize low index
    int highIn = grades.length - 1;//initialize high index 
    int i = 0;//initialize index for number of iterations 
    while(highIn >= lowIn){
      i++;//count the iterations
      int midIn = (lowIn + highIn)/2;//get position of middle index 
      if(s < grades[midIn]){//when int to search is between low int and mid int 
        highIn = midIn - 1;
      }else if(s == grades[midIn]){//when int to search is at mid 
        System.out.println(s +" was found in the list with "+ i +" iterations.");
        return;//end method 
      }else{//when int to search is between mid int and high int 
        lowIn = midIn + 1;
      }
    }//loop ends when low index > high index 
    System.out.println(s +" was not found in the list with "+ i +" iterations.");
    return;//end method 
  }
  
  public static void LiSearch(int[] grades,int s){
    int i = 0;//index for looping 
    for(i = 0; i < grades.length; i++){//linear search from first int to last int 
      if(s == grades[i]){//when int found 
        i++;//count the iterations
        System.out.println(s +" was found in the list with "+ i +" iterations.");
        return;//end method  
      }
    }
    i++;//count the iterations 
    System.out.println(s +" was not found in the list with "+ i +" iterations.");
    return;//end method 
  }
  
  public static int[] Shuffle(int[] grades){//shuffle the integers in the list 
    Random rand = new Random();//for generating random number 
    for (int i = 0; i < grades.length; i++){
		    int position = rand.nextInt(grades.length);//get random position in the list 
		    int temp = grades[i];//get the integer on the i-th place on the list 
		    grades[i] = grades[position];//int on the i-th place is replaced by int on the random position 
		    grades[position] = temp;//int on the position replaced by int on the i-th place 
		}
    return grades;//return the array 
	}
}