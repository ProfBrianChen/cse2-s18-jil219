/*
CSE2 Lab5 JIAHUI LI
Accept user input(integer)
and print out three rows 
with pattern
*/
import java.util.Scanner;

public class TwistGenerator{
    //main method required for every java program
public static void main(String[] args){
  //prompt user for input
  Scanner myScanner = new Scanner(System.in);

  int counterI = 0;//counter for looping for correct input 
  int counterO = 1;//counter for looping to output 
  String line1 = " ";//output for the first line
  String line2 = " ";//output for the second line
  String line3 = " ";//output for the third line
  
  //loop for different conditions
  while (counterI == 0){
    System.out.print("Please enter the length of the twist: ");
    //if user input is not an integer 
    if(!myScanner.hasNextInt()){
      System.out.println("Please enter the length again: ");
      myScanner.next();//remove user input 
    //if user input is an integer 
    }else if(myScanner.hasNextInt()){
      int twistL = myScanner.nextInt();//assign user input to the length of twist

      //if user input is not positive 
      if(twistL <= 0){
        System.out.println("Please enter the length again: ");
      //if user input is positive 
      }else if(twistL > 0){
        counterI += 1;//does not loop when length is positive 
        int overThree = twistL / 3;//initializes integer of length over three 
        int rev = twistL % 3;//initializes remainder of length over 3
        //overThree is the number of group of three spaces that needs to be display 
        while(counterO <= overThree){
          line1 += "\\ /";
          line2 +=" X ";
          line3 += "/ \\";
          counterO++;//counterO adds 1 until all groups of three are added
        }
        //if the remainder of length over 3 is 1
        if(rev == 1){
          line1 += "\\";
          line2 += " ";
          line3 += "/";
        }
        //if the remainder of length over 3 is 2
        if(rev == 2){
          line1 += " ";
          line2 += "X";
          line3 += " ";
        }     
        System.out.println(line1);//print the first line
        System.out.println(line2);//print the second line
        System.out.println(line3);//print the third line
      }
    }
  }
 }
}