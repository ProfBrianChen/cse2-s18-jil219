/*
CSE2 HW3 JIAHUI LI
Prompts the user for the dimensions of a pyramid 
and returns the volume inside the pyramid
*/

import java.util.Scanner;

public class Pyramid {

  public static void main(String[] args){
    
    //declare scanner object & call scanner constructor 
    Scanner myScanner = new Scanner(System.in);
    
    //prompt user for the length and width of the square side of the pyramid
    System.out.print("The square side of the pyramid is (input length): ");
    //accept user's input for length
    double pL = myScanner.nextDouble();
    
    //prompt user for the height of the pyramid
    System.out.print("The height of the pyramid is (input height): ");
    //accept user's input for height 
    double pH = myScanner.nextDouble();
    
    //calculates the volume of the pyramid
    double pV = pL*pL*pH/3;
    //output the volume of the pyramid 
    System.out.println("The volume inside the pyramid is: " + pV);

  }
  
}