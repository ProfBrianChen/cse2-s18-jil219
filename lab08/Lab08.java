/////////////////////////
//CSE2 LAB8 JIAHUI LI
//generate random number of students 
//assign random grades 
////////////////////////

import java.util.Scanner;

public class Lab08{//class 
  
    public static void main(String[] args){//main method
      
      int numS = (int)(Math.random()*6)+5; //generate random number of students
      String[] stud = new String[numS];//create an array of students
      int[] grade = new int[numS];//create an array of grades
      Scanner scan = new Scanner(System.in);
      System.out.println("Enter " + numS + " students names: ");//ask user for students' names
      for(int i=0; i<numS ; i++){//store students names in array 
        stud[i] = scan.next();//accept user input 
      }
      for(int j=0; j<numS ; j++){//store grades in array 
        grade[j]= (int)(Math.random()*100);//generate a random grade from 0 - 100
      }
      System.out.println("Here are the midterm grades of the " + numS + " students above: ");
      
      for(int k=0; k<numS ; k++){
          System.out.println(stud[k]+" : "+ grade[k]);//print the list
      }
    }
}// end of class