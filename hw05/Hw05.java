///////////////////////////////
//CSE2 HW5 JIAHUI LI
//Asks for the course number, 
//department name, the number 
//of times it meets in a week, 
//the time the class starts, 
//the instructor name, 
//and the number of students
///////////////////////////////

import java.util.Scanner;

public class Hw05{
  //main method required for every java program 
  public static void main (String[] args){
    //declare scanner object
    Scanner myScanner = new Scanner(System.in);
    //variable for continuing looping 
    int i = 0;
    
    //initialize variable for course number
    int courseNum = 0;//course number is in integer
    //loop to ask for course number
    while(i == 0){
      //prompt user for course number 
      System.out.print("Please enter the course number.\n");
      //if the input is not an integer
      if(!myScanner.hasNextInt()){
        myScanner.next();//remove input
        continue;
      }
      //course number is the user input 
      courseNum = myScanner.nextInt();
      //if user input is negative integer
      if(courseNum < 0){
        System.out.println("Cannot be negative.");//informs user 
        myScanner.next();//remove input
        continue;
      //if input is a positive integer 
      }else{
        System.out.println("Correct. Thank you.");
        i += 1;//ends this loop, continue to the next loop
      }
    }
    //initialize variable for department name
    String departName = "dname";//department name is a string of character 
    //i is 1 if course number is in correct format 
    while(i == 1){
      //prompt user for department name 
      System.out.print("Please enter the department name.\n");
      //department name cannot be a number
      if(myScanner.hasNextInt() || myScanner.hasNextDouble()){
        System.out.println("Cannot be a number.");//informs user
        myScanner.next();//remove input
        continue;//ends iteration 
      }
      //if the input is not a string 
      if(!myScanner.hasNextLine()){
        myScanner.next();//remove input
        continue;
      //if the input is a string 
      }else{
        //department name is the user input 
        departName = myScanner.nextLine();
        System.out.println("Correct. Thank you.");
        i += 1;//ends the loop, continue to next loop 
      }
    }
    //initialize integer variable for frequency 
    int timesaWeek = 0;
    //i is 2 if the department name is in correct format
    while(i == 2){
      //prompt user for frequency in a week
      System.out.print("Please enter the number of times it meets in a week.\n");
      //if user input is not an integer 
      if(!myScanner.hasNextInt()){
        myScanner.next();//remove input
        continue;
      }
      //user input is the frequency
      timesaWeek = myScanner.nextInt();
      //if user input is negative 
      if(timesaWeek <= 0){
        System.out.println("Needs to be positive.");//informs user 
        myScanner.next();//remove user input 
        continue;//ends iteration
      //if user input is in correct format
      }else{
        System.out.println("Correct. Thank you.");
        i += 1;//ends the loop
      }
    }
    //initialize the variable for starting time as a double
    double hStart = 0.00;
    //i is 3 if frequency is in correct format
    while(i == 3){
      //prompt user for starting time
      System.out.print("Please enter the time the class starts(hh.mm).\n");
      //if user input is not a double 
      if(!myScanner.hasNextDouble()){
        myScanner.next();//remove input
        continue;
      }
      //starting time is user input
      hStart = myScanner.nextDouble();
      //hours cannot be greater than 24
      if(hStart > 24.00){
        System.out.println("Hour can't be greater than 24");
        myScanner.next();//remove user input
        continue;//ends iteration
      //if user input is a double
      }else{
        System.out.println("Correct. Thank you.");
        i += 1;//ends loop
      }
    }
    //initialize instructor's name
    String insName = "iname";
    //i is 4 if starting time is in correct format
    while(i == 4){
      //prompt user for instructor's name
      System.out.print("Please enter the instructor's name.\n");
      //if a number is entered 
       if(myScanner.hasNextInt() || myScanner.hasNextDouble()){
        System.out.println("Cannot be a number.");//informs user
        myScanner.next();//remove input
        continue;//ends iteration 
      }
      //if input is not a string 
      if(!myScanner.hasNextLine()){
        myScanner.next();//remove input 
        continue;
      //if input is a string
      }else{
        //user input is the instructor's name
        insName = myScanner.nextLine();
        System.out.println("Correct. Thank you.");
        i += 1;//ends loop
      }
    }
    //initialize number of students
    int studNum = 0;
    //i is 5 if instructor's name is in correct format
    while(i == 5){
      //prompt user for number of students
      System.out.print("Please enter the number of students.\n");
      //if user input is not an integer 
      if(!myScanner.hasNextInt()){
        myScanner.next();//remove input
        continue;
      }
      //user input is number of students
      studNum = myScanner.nextInt();
      //if user input is negative
      if(studNum <= 0){
        System.out.println("Needs to be positive.");//informs user 
        myScanner.next();//remove input 
        continue;
      //if user input is an integer 
      }else{
        System.out.println("Correct. Thank you.");
        i += 1;//ends loop
      }
    }
    
    
  }//end of main method
}//end of class