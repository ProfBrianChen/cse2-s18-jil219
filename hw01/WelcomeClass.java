/*
CSE2 HW01 JIAHUI LI
*/

public class WelcomeClass{
  
  public static void main(String[] args){
    // Prints "WELCOME" and LehighID
    
    System.out.println("  -----------  "); //1st
    System.out.println("  | WELCOME |  "); //2nd
    System.out.println("  -----------  "); //3rd
    System.out.println("  ^  ^  ^  ^  ^  ^  "); //4th
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ "); //5th
    System.out.println("<-J--I--L--2--1--9->"); //6th
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / "); //7th
    System.out.println("  v  v  v  v  v  v  "); //8th
    
  }
  
}