///////////////////////////////
//CSE2 LAB10 JIAHUI LI
//
//
/////////////////////////////

import java.util.Random;
import java.util.Scanner;

public class Lab10{
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    System.out.print("Enter the width and the height of the matrix: ");
    int w = scan.nextInt();//width of matrix is number of columns
    int h = scan.nextInt();//height of matrix is number of rows
    
    System.out.print("Row-major (y/n)? ");
    String major = scan.next();
    boolean format = false;
    if(major.equals("y")){
      format = true;
    }
    if(major.equals("n")){
      format = false;
    }
    int[][] matrix = increasingMatrix(w,h,format);
    System.out.println("Generating a matrix with width " + 
                       w + " and height " + h);   
    printMatrix(matrix,format); 
    
  }
  public static void printMatrix(int[][] array,boolean form){
    if(form == true){
      for(int i=0; i<array.length; i++){
        System.out.print("[");
        for(int j=0; j<array[i].length; j++){
          System.out.printf("%1000d",array[i][j]);
        }
        System.out.print("]");
        System.out.println();
      }}
    if(form == false){
      for(int j=0; j<array[0].length; j++){//column matrix 
        System.out.print("[");
        for(int i=0; i<array.length; i++){//row number 
          System.out.printf("%1000d",array[i][j]);
        }
        System.out.print("]");
        System.out.println();
      }
    }
  }
  public static int[][] increasingMatrix(int column,int row,boolean form){
    int[][] array = new int[row][column];
    if(form == true){
      int val = 1;
      for(int i=0; i<row; i++){
        array[i] = new int[column];
        for(int j=0; j<column; j++){
          array[i][j] = val;
          val++;
        }}
    }else if(form == false){
      array = new int[column][row];
      int val = 1;
      for(int j=0; j<column; j++){//array for each column
        array[j] = new int[row];
        for(int i=0; i<row; i++){
          array[j][i] = val + (i * column);
        }
        val++;
      }
    }
    return array;
  }
}