///////////////////////////
//CSE2 HW07-2 JIAHUI LI
//User input a string
//and determine how many
//characters to examine
/////////////////////////

import java.util.Scanner;

public class StringAnalysis{//beginning of class
    public static void main(String[] args){//beginning of main method
    Scanner myScanner = new Scanner(System.in);//declare scanner object 
      
    String userIn = " ";//initialize user input for string 
    while(true){//loop for checking user input
      System.out.print("Please enter a string: ");//prompt user for input
      if(!myScanner.hasNextLine()){//if user input is not a string
        myScanner.next();//clear input
      }
      else if(myScanner.hasNextLine()){//if user input is a string
        userIn = myScanner.next();//accept user input
        break;//ends loop
      }
    }
    String check = " ";//initialize user input for how many characters to check
    while (true){//loop for asking user number of characters to check 
      System.out.print("Would you like to check all the characters?(yes/no) ");//ask user to choose 
      if(!myScanner.hasNextLine()){//if user input isn't string
        myScanner.next();//clear user input
      }
      else if(myScanner.hasNextLine()){//if input is string, check if user enters yes or no
        check = myScanner.next();//accept input 
        if(check.equals("yes")){//if user wants to check all the characters
          boolean analysis = isLetter(userIn);//invoke the method for checking all characters
          if(analysis == true){//if all are letters 
            System.out.println("All are letters.");
            break;//ends looping 
          }
          else if(analysis == false){//if some are not letters 
            System.out.println("Not all letters.");
            break;//ends looping 
          }
        }else if(check.equals("no")){//if user doesn't want to check all characters
          int numCheck = 0;//initialize user input for how many characters to check 
          while(true){//loop to check user input 
            System.out.print("How many characters would you like to check? ");//prompt user for input 
            if(!myScanner.hasNextInt()){//if user input isn't integer 
              System.out.println("Please enter an integer.\n");//ask user to type in integer 
              myScanner.next();//clear user input
            }
            else if(myScanner.hasNextInt()){//if user input is integer
              numCheck = myScanner.nextInt();//accept user input 
              if(numCheck <= 0){//user input can't be negative
                System.out.println("Please enter a positive integer.\n");
              }else{//if user input is positive 
                break;//ends looping 
              }
            }
          }//end of loop for checking numbers of characters to check 
          boolean analysis = isLetter(numCheck,userIn);//invoke method for checking certain number of characters
          if(analysis == true){//when all are letters
            System.out.println("All are letters.");
            break;//ends looping
          }
          else if(analysis == false){//when some are not letters 
            System.out.println("Not all letters.");
            break;//ends looping
          }
        }else{//if user input isn't 'yes' or 'no'
          System.out.println("Please enter 'yes' or 'no'.\n");
        }
      }
    }//ends of loop for checking string 
  }//end of main method 
  
  public static boolean isLetter(String userIn){//method for checking all characters 
    boolean accept = true;//initialize boolean for checking if the string is all letters 
    for(int i = 0;i < userIn.length();i++){//loop to check each character in the string 
      if(userIn.charAt(i) >= 'a' && userIn.charAt(i) <= 'z'){//when each character is between 'a' and 'z'
        accept = true;//all are letters is true 
      }else{//when some are not letters
        accept = false;
      }
    }
    return accept;//save the boolean value 
  }//end of method
  
  public static boolean isLetter(int numCheck, String userIn){//method for checking number of characters
    boolean accept = true;//initialize boolean for checking if the part of the string is all letters 
    for(int i = 0;i < userIn.length() && i < numCheck; i++){//loop to check certain number of characters
      if(userIn.charAt(i) >= 'a' && userIn.charAt(i) <= 'z'){//when all are letters 
        accept = true;
      }else{//when some are not letters 
        accept = false;
      }
    }
    return accept;//save the boolean value 
  }//end of method
  
}//end of class