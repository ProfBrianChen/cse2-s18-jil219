////////////////////////
//CSE2 HW09 JIAHUI LI
//Draw poker game
//two players
////////////////////////

public class DrawPoker{
    public static void main(String[] args){
      String[] suits={"Diamond","Club","Heart","Spade"};//create the list of four suits   
      String[] ranks={"A","2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K"};//create the list of card numbers
      int[] cards = new int[52];//card numbers
      int[] randomC = new int[52];//cards after shuffling
      int[] player1 = new int[5];
      int[] player2 = new int[5];//players cards 
      String[] rank1 = new String[5];
      String[] rank2 = new String[5];//ranks of players cards 
      String[] suit1 = new String[5];
      String[] suit2 = new String[5];//suits of players cards 
  
      for (int i=0; i<52; i++){
       cards[i]= i;//ranks[i%13]+suits[i/13]
      }
      randomC = shuffle(cards);//call method to shuffle cards

      int i = 0;
      while (i<10){//assign cards to each player 
        for (int j = 0; j < 5; j++){//each player has 5 cards 
          player1[j] = randomC[i];
          rank1[j] = ranks[player1[j]%13];
          suit1[j] = suits[player1[j]/13];
          System.out.println("Player One has " + rank1[j] +" of "+ suit1[j]);//output ranks and suits 
          i++;//go to the next card 
      
          player2[j] = randomC[i];
          rank2[j] = ranks[player2[j]%13];
          suit2[j] = suits[player2[j]/13];
          System.out.println("Player Two has " + rank2[j] +" of "+ suit2[j]);//output ranks and suits
          i++;//go to the next card 
        }
      }
      int s1 = 0, s2 = 0;//compare players hands 
      if(pair(rank1)){s1=1;} 
      if(pair(rank2)){s2=1;}//pair gets lowest score
      if(three(rank1)){s1=2;}
      if(three(rank2)){s2=2;}//three of a kind scores higher than pair 
      if(flush(suit1)){s1=3;}
      if(flush(suit2)){s2=3;}//flush scores higher than flush
      if(full(rank1)){s1=4;}
      if(full(rank2)){s2=4;}//full scores highest 
      if(!pair(rank1)&&!pair(rank2)&&!three(rank1)&&!three(rank2)&&!flush(suit1)&&!flush(suit2)&&!full(rank1)&&!full(rank2)){
        s1=high(player1);
        s2=high(player2);
      }//when none has special hands, compare the highest 
	  	result(s1,s2);//call method to compare scores
    }    
    
    public static int[] shuffle(int[] cards) {//shuffle the cards
        for(int i=0 ; i<cards.length ; i++){//swap cards in pairs of two
            int randC = (int)(Math.random()*51)+1;//generate a random card number
            int temp = cards[i];
            cards[i] = cards[randC];
            cards[randC] = temp;
        }
      return cards;//return list after shuffling
    }
    
    public static boolean pair(String[] list){//check if there is pair 
        boolean pair = false;//declare boolean 
        for(int i=0; i<list.length; i++ ){
            for(int j=1; j<list.length; j++){
                if(list[i].equals(list[j])){//if two cards are the same 
                   pair = true;//there is pair
                }}
        }
        return pair;//return boolean 
    }
    
    public static boolean three(String[] list){//check three of a kind 
        boolean three = false;//declare boolean
        for(int i=0; i<list.length; i++){
            for(int j=1; j<list.length; j++){
                for(int k=2; k<list.length; k++){
                if(list[i].equals(list[j]) && list[i].equals(list[k])){
                   three = true;
                }}}
        }//check if there are three cards of the same rank 
        return three;//return boolean 
    }

    public static boolean flush(String[] list){
        boolean flush = false;
        int val = 0;//index for checking equal suits  
        for(int i=0; i<list.length; i++){
            for(int j=1; j<list.length; j++){
                if(list[i].equals(list[j])){
                    val+=1;//increment val if two suits the same 
                }}
        }
        if(val==20){//5 rank[i] compared to 4 rank[j]
            flush=true;
        }
        return flush;//return boolean  
    }
    
    public static boolean full(String[] list){//check full house 
        boolean full = false;
        int val = 0;//index for checking equal ranks
        for(int i=0; i<list.length; i++ ){
            for(int j=1; j<list.length; j++){
                if(list[i].equals(list[j])){
                    val+=1;//increment 1 if two ranks the same 
                }}
        }
        if(val==10||val==11){//when there is full house 
            full = true;
        }
        return full;//return boolean
    }

    public static int high(int[] list){//find the highest card
        int max = 0;
        for(int i=0; i<list.length; i++){
          if(list[i]<13){//assign the highest rank to the max value 
            max = list[i];
          }else if(13<=list[i]&&list[i]<26){
            max = list[i]-13;//assign equivalent value of rank 
          }else if(26<=list[i]&&list[i]<39){
            max = list[i]-26;//assign equivalent value of rank
          }else if(39<=list[i]&&list[i]<52){
            max = list[i]-39;//assign equivalent value of rank 
          }
        }
        return max;//return highest value 
    }
    
    public static void result(int a,int b){//compare scores of two players
        if(a>b){//when player1 scores higher
            System.out.println("Player One wins.");
        }else if(b<a){//when player2 scores higher 
            System.out.println("Player Two wins.");
        }else{//when tied
            System.out.println("Tied.");
        }
    }    
}