/////////////////////////
//CSE2 HW07-1 JIAHUI LI
//User input shapes and side lengths
//of rectangle, triangle, or circle
//calculates the area
/////////////////////////

import java.util.Scanner;//imports scanner

public class Area{
  //main method required for every java program 
  public static void main(String[] args){
    //use boolean to check user input 
    boolean accept = acceptIn();
    while (!accept){
    //loop to ask for user input when input is not acceptable 
      accept = acceptIn();
    }
  }
  //method for checking user input
  public static boolean acceptIn(){
    boolean acceptable;//declare boolean for whether the shape is acceptable or not
    Scanner myScanner = new Scanner(System.in);//declare scanner object 
    System.out.print("Please enter a shape (rectangle/triangle/circle): ");//ask user for input 
    String userIn = myScanner.next();//accept user input
    if (userIn.equals("rectangle")){
      acceptable = true;//user input rectangle is acceptable
      double rectangle = rec();//invoke method for rectangle
    }else if(userIn.equals("triangle")){
      acceptable = true;//user input triangle is acceptable
      double triangle = tri();//invoke method for triangle
    }else if(userIn.equals("circle")){
      acceptable = true;//user input circle is acceptable
      double circle = cir();//invoke method for circle
    }else{
      //when user input anything else
      System.out.println("Invalid Input. Only rectangle, triangle, and circle are acceptable.\n");
      acceptable = false;//input is not acceptable
    }
    return acceptable;//return boolean value for checking user input 
  }
  //method for calculating area of rectangle 
  public static double rec(){
    double width = 0.0;//initialize width of the rectangle
    double length = 0.0;//initialize length of the rectangle
    Scanner myScanner = new Scanner(System.in);//declare scanner object
    
    //loop to obtain user input for width 
    while(true){
      System.out.print("Please enter the width: ");//ask user for input
      //when user input is not a double
      if(!myScanner.hasNextDouble()){
        System.out.println("Please enter a double.\n");//inform user to enter again
        myScanner.next();//remove user input
      }
      //when user enters a double
      else if(myScanner.hasNextDouble()){
        width = myScanner.nextDouble();//accept user input
        if(width <= 0){//if user input is negative 
          System.out.println("Please enter a positive number.\n");//inform user to enter again
        }else{//if user input is positive
          break;//ends the loop
        }
      }
    }
    //loop to obtain user input for length 
    while(true){
      System.out.print("Please enter the length: ");//ask user for input 
      //when user input is not a double
      if(!myScanner.hasNextDouble()){
        System.out.println("Please enter a double.\n");//inform user to enter again 
        myScanner.next();//remove user input 
      }
      //when user input is double
      else if(myScanner.hasNextDouble()){
        length = myScanner.nextDouble();//accept user input 
        if(length <= 0){//if user input is negative
          System.out.println("Please enter a positive number.\n");//inform user to enter again 
        }else{//if user input is positive 
          break;//ends the loop
        }
      }
    }
    double recArea = width * length;//calculates the area of a rectangle
    System.out.println("Area of the rectangle is " + recArea);//output the result 
    return recArea;//save the area of the rectangle
    }
  //method for calculating area of triangle
  public static double tri(){
    double base = 0.0;//initialize base of the triangle
    double height = 0.0;//initialize height of the triangle 
    Scanner myScanner = new Scanner(System.in);//declare scanner object 
    //loop to ask user for input for base 
    while (true){
      System.out.print("Please enter the length of the base: ");//ask for input
      //when user input is not a double 
      if(!myScanner.hasNextDouble()){
        System.out.println("Please enter a double.\n");//inform user to enter again 
        myScanner.next();//remove user input 
      }
      //when user input is a double 
      else if(myScanner.hasNextDouble()){
        base = myScanner.nextDouble();//accept user input 
        if(base <= 0){//if user input is negative
          System.out.println("Please enter a positive number.\n");//inform user to enter again 
        }else{//if user input is positive
          break;//ends the loop
        }
      }
    }
    //loop to ask user for input for height 
    while(true){
      System.out.print("Please enter the height: ");//ask user for input 
      //when user input is not a double 
      if (!myScanner.hasNextDouble()){
        System.out.println("Please enter a double.\n");//inform user to enter again
        myScanner.next();//remove input 
      }
      //when user input is a double 
      else if(myScanner.hasNextDouble()){
        height = myScanner.nextDouble();//accept user input 
        if(height <= 0){//if input is negative 
          System.out.println("Please enter a positive number.\n");//inform user to enter again 
        }else{//if input is positive 
          break;//ends the loop
        }
      }
    }
    double triArea = (base * height) / 2;//calculates area of a triangle 
    System.out.println("Area of the triangle is " + triArea);//output the result 
    return triArea;//save the area of the triangle 
  }
  //method for calculating area of circle 
  public static double cir(){
    double radius = 0;//initialize radius of circle 
    Scanner myScanner = new Scanner(System.in);//declare scanner object
    //ask user for input for radius 
    while(true){
      System.out.print("Please enter the radius: ");//ask for input
      //if user input is not a double 
      if(!myScanner.hasNextDouble()){
        System.out.println("Please enter a double.\n");//inform user to enter again 
        myScanner.next();//remove input
      }
      //if user input is a double 
      else if(myScanner.hasNextDouble()){
        radius = myScanner.nextDouble();//accept user input 
        if(radius <= 0){//if input is negative
          System.out.println("Please enter a positive number.\n");//inform user to enter again 
        }else{//if input is positive 
          break;//ends the loop
        }
      }
    }
    double cirArea = (Math.PI) * (Math.pow(radius,2.0));//calculates area of a circle 
    System.out.println("Area of the circle is " + cirArea);//output the result 
    return cirArea;//save the area of the circle
  }
    
}//end of class