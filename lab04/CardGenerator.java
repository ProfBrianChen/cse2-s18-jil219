/*
CSE2 LAB04 JIAHUI LI
Program will pick a random card from the deck. 
Output the rank and the suit of the card.  
*/

public class CardGenerator {
  
  public static void main (String[] args) {
      //generates a random number in the range of 1 to 52, inclusive 
      int randomCard = (int)Math.floor((Math.random() * 52) + 1);
      String suit; //declare String variable for the name of suit
      String rank; //declare String variable for the identity of card
      //calculates the quotient as an integer, 
      //if the random number is equal to 13,
      //the suit is Diamond, 
      //(int)(13 / 13) = 1 will make the suit "Club"
      //(int)((13 - 1) / 13) = 0 makes the suit "Diamond"
      int quo = (int)((randomCard - 1) / 13);
      
      if(quo==0){
        //"Diamond" from 1 to 13
        suit = "Diamond";
      }else if(quo==1){
        //"Club" from 14 to 26
        suit = "Club";
      }else if(quo==2){
        //"Heart" from 27 to 39
        suit = "Heart";
      }else if(quo==3){
        //"Spade" from 40 to 52
        suit = "Spade";
      }else{
        //if quotient is not 0, 1, 2, or 3
        suit = "Error";
      }
      //calculates the modulus as an integer,
      //if the random number is less than 13,
      //(randomCard % 13) will equal to the random number itself
      int mod = (int)((randomCard + 13) % 13);
      
      switch(mod){
        //switch on the rank of the random card using the modulus 
        case 0: rank = "King"; break;
        case 1: rank = "Ace"; break;
        case 11: rank = "Jack"; break;
        case 12: rank = "Queen"; break;
        //for 2 up to 10, ranks are equal
        //to their own modulus 
        default: rank = "" + mod; break; 
      }
      //output rank and suit 
      System.out.println("You picked the " + rank 
                       + " of " + suit);
    
  }
  
}