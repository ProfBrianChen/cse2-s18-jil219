/*
CSE2 HW3 JIAHUI LI
Asks the user for doubles that represent the 
number of acres of land affected by hurricane 
precipitation and how many inches of rain were dropped on average.
Convert the quantity of rain into cubic miles.
*/

import java.util.Scanner;

public class Convert {

  public static void main(String[] args){
    
    //declare scanner object & call scanner constructor 
    Scanner myScanner = new Scanner(System.in);
    
    //prompt user for number of acres of land affected by hurricane 
    System.out.print("Enter the affected area in acres: ");
    //accept user input for acres of land 
    double acresofLand = myScanner.nextDouble();
    
    //prompt user for inches of rainfall on average 
    System.out.print("Enter the rainfall in the affected area: ");
    //accept user input for inches of rainfall 
    double inchesofRain = myScanner.nextDouble();
    
    //1 inch of rain on 1 acre of ground is about 27154 gallons
    //calculates the gallons of rain on the affected land
    double gallonsofRain = 27154 * inchesofRain * acresofLand;
    
    //x is 1 gallon in terms of cubic miles 
    double x = 9.08169 * Math.pow(10,-13);
    //convert gallons of rain to cubic miles of rain 
    double cubicmisofRain = x * gallonsofRain;
    //output the amount of rain dropped in affected area in cubic miles 
    System.out.println(cubicmisofRain + " cubic miles ");
    
  }
  
}
