////////////////////////////
//CSE2 JIAHUI LI
//Uses the Scanner class to obtain from the user the original cost of the check,
//the percentage tip they wish to pay,
//and the number of ways the check will be split.
//Output how much each person in the group needs to spend in order to pay the check.
/////

import java.util.Scanner;

public class Check {
  
  public static void main(String[] args){
    
    //declare Scanner object and call Scanner constructor
    Scanner myScanner = new Scanner(System.in);
    
    //prompt user for original cost of the check 
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    //accept user input 
    double checkCost = myScanner.nextDouble();
    
    //prompt user for tip percentage and accept input
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; //convert the percentage into a decimal value

    
    //prompt user for the number of people that went to dinner and accept the input
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies; //for whole dollar amount of cost & for storing digits to the right of the decimal point
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    //get the whole amount, dropping decimal fraction
    dollars = (int)costPerPerson;
    //get dimes amount, e.g.,
    // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
    //  where the % (mod) operator returns the remainder
    //  after the division:   583%100 -> 83, 27%5 -> 2
    dimes=(int)(costPerPerson * 10) % 10;
    pennies=(int)(costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
    
  } //end of main method 
  
} //end of class