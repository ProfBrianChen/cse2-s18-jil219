///////////////////////
//CSE2 HW2 JIAHUI LI
///

public class Arithmetic{
  //Calculates the prices of clothes with tax 
  public static void main (String[] args){    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    //Number of belts
    int numBelts = 1;
    //Cost per belt 
    double beltCost = 33.99;
    //the tax rate
    double paSalesTax = 0.06;
    
    //Calculates output data
    double pantsTotal = (double)(numPants * pantsPrice); //total price of pants before tax
    double pantsTax = (int)(pantsTotal * paSalesTax * 100); //tax for pants (times 100 for rounding) 
    double shirtsTotal = (double)(numShirts * shirtPrice); //total price of shirts before tax
    double shirtsTax = (int)(shirtsTotal * paSalesTax * 100); //tax for shirts 
    double beltsTotal = (double)(numBelts * beltCost); //total price of belts
    double beltsTax = (int)(beltsTotal * paSalesTax * 100); //tax for belts
    double purchaseTotal = pantsTotal + shirtsTotal + beltsTotal; //total cost of purchase before tax
    double taxTotal = (pantsTax + shirtsTax + beltsTax)/100; //total tax price
    double Total = purchaseTotal + taxTotal; //total paid in this transaction,including sales tax
    
    //Prints output data
    System.out.println("Total price of Pants: $" + pantsTotal + 
                       "\n" + "Total price of Shirts: $" + shirtsTotal + 
                       "\n" + "Total price of Belts: $" + beltsTotal); //prints out totals for pants,shirts,and belts
    System.out.println("Tax for Pants: $" + pantsTax/100 + 
                       "\n" + "Tax for Shirts: $" + shirtsTax/100 + 
                       "\n" + "Tax for Belts: $" + beltsTax/100); //prints out taxes for pants,shirts,and belts
    System.out.println("\n" + "Total cost of purchase before tax: $" + purchaseTotal);
    System.out.println("Total sales tax: $" + taxTotal);
    System.out.println("Total including sales tax: $" + Total);
    
  }
  
}