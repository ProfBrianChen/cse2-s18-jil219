/////////////////////////
//CSE2 LAB09 JIAHUI LI
//
//
/////////////////////////

public class Lab09{
    public static void main(String[] args){
        int[] array0 = new int[10];
        for (int i=0; i< array0.length; i++){
            array0[i] = i;
        }
        int[] array1 = copy(array0);
        int[] array2 = copy(array0);
        System.out.println("Array0: ");
        inverter(array0);
        print(array0);
        
        inverter2(array1);
        System.out.println("\n"+"Array1:");
        print(array1);
        
        System.out.println("\n"+"Array3:");
        int[] array3 = inverter2(array2);
        print(array3);
        
        System.out.println();
    }
    public static int[] copy(int[] list){
        int[] result = new int[list.length];
        for(int i=0; i < list.length; i++){
            result[i] = list[i];
        }
        return result;
    }
    public static void inverter(int[] list){
        int val = list.length;
        for (int i=0; i < (val/2); i++){
            int temp = list[i];
            list[i] = list[val - i - 1];
            list[val - i - 1] = temp;   
        }
    }
    public static int[] inverter2(int[] list){
        int[] result = copy(list);
        inverter(result);
        return result;
    }
      public static void print(int[] list){
      for (int i=0; i < list.length; i++){
        System.out.print(list[i] + ",");  
        }
    }
    
}