////////////////////////
//CSE2 HW06 JIAHUI LI
//Argyle
//Generates Argyle

import java.util.Scanner;

public class Argyle{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    int counter = 0;
    
    int winW = 0;
    int winH = 0;
    int diaW = 0;
    int strW = 0;
    char fill1 = 'a';
    char fill2 = 'b';
    char fill3 = 'c';

    while(counter == 0){
      System.out.print("Please enter a positive integer for the width of viewing window.\n");
      if(!myScanner.hasNextInt()){
        myScanner.next();
        //continue;
      }
      String input = myScanner.next();
      char result = input.charAt(0);
      winW = Integer.valueOf(input);
      if(result == '-'){
        System.out.println("Needs to be positive.\n");
      }else{
        counter += 1;
      }
    }
    //System.out.println(winW);

      
    while(counter == 1){
      System.out.print("Please enter a positive integer for the height of viewing window.\n");
      if(!myScanner.hasNextInt()){
        myScanner.next();
        //continue;
      }
      String input = myScanner.next();
      char result = input.charAt(0);
      winH = Integer.valueOf(input);
      if(result == '-'){
        System.out.println("Needs to be positive.\n");
      }else{
        counter += 1;
      }
    }
    //System.out.println(winH);
      
    while(counter == 2){
      System.out.print("Please enter a positive integer for the width of the argyle diamonds.\n");
      if(!myScanner.hasNextInt()){
        myScanner.next();
        //continue;
      }
      diaW = myScanner.nextInt();
      if(diaW <= 0){
        System.out.println("Needs to be positive.\n");
      }else{
        counter += 1;
      }
    }
    //System.out.println(diaW);
      
    while(counter == 3){
      System.out.print("Please enter a positive odd integer for the width of the argyle center stripe.\n");
      if(!myScanner.hasNextInt()){
        myScanner.next();
        //continue;
      }
      strW = myScanner.nextInt();
      if(strW > (diaW/2) || (strW < 0) || (strW%2 == 0)){
        System.out.println("Need a positive odd integer not larger than half of the diamond size.\n");
      }else{
        counter += 1;
      }
    }
    //System.out.println(strW);
      
    while(counter == 4){
      System.out.print("Please enter a first character for the pattern fill.\n");
      String input = myScanner.next();
      if(input.length() > 1){
        System.out.println("One character only.\n");
      }else{      
        fill1 = input.charAt(0);
        counter += 1;
      }
    }
    //System.out.println(fill1);
      
    while(counter == 5){
      System.out.print("Please enter a second character for the pattern fill.\n");
      String input = myScanner.next();
      if(input.length() > 1){
        System.out.println("One character only.\n");
      }else{
        fill2 = input.charAt(0);
        counter += 1;
      }
    }
    //System.out.println(fill2);

    while(counter == 6){
      System.out.print("Please enter a third character for the stripe fill.\n");
      String input = myScanner.next();
      if(input.length() > 1){
        System.out.println("One character only.\n");
      }else{
        fill3 = input.charAt(0);
        counter += 1;
      }
    }
    //System.out.println(fill3);

    
  }//end of method 
}//end of class