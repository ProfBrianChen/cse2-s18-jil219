//////////////////////////////
//CSE2 LAB06 JIAHUI LI
//Generate 'x' encrypted
//in multiple '*'
//side length decided by user
//////////////////////////////

import java.util.Scanner;

public class encrypted_x{
  //main method required for every Java program
  public static void main(String[] args) {
    int side = 0;//initialize the length
    Scanner myScanner = new Scanner(System.in);//declare scanner object 
    while(true){//loop to check user input for length 
      System.out.print("Input your desired length: ");
      if(!myScanner.hasNextInt()){ //if the user didn't enter an integer
        System.out.println("Please enter an integer.\n");
        myScanner.next();//remove user input 
      }
      else if(myScanner.hasNextInt()){//when user input is integer 
        side = myScanner.nextInt();//accept user input
        if(side > 100){//when input is over 100
          System.out.println("Length can't be greater than 100.\n");
        }else if(side <= 0){//when input isn't positive
          System.out.println("Please enter a positive integer.\n");
        }else{//when the integer is between 0 and 100
          break;//ends looping
        }
      }
    }
    
    int rev1 = 1;//remainder for upper left section spaces
    int rev2 = side;//remainder for upper right spaces
    int rev3 = side;//remainder for lower left spaces
    int rev4 = side - 1;//remainder for lower right spaces
    
    System.out.println("\nFor Loop");
    for(int i = 0;i < side;i++){
        System.out.println("");//print number of lines that equal to user input length 
        for(int j = 1;j <= side;j++){
            if(j % (2*side) == rev1){
                System.out.print(" ");//print out the space on the upper left section 
            }else if((j-1) % (2*side) == rev2){
                System.out.print (" ");//print out the space on the upper right section
            }else if(j % (2*side) == rev3){
                System.out.print(" ");//print out the space on the lower left section
            }else if((j-1) % side == rev4){
                System.out.print(" ");//print out the space on the lower right section 
            }else{
                System.out.print("*");//fill all the other with *
            }
        }
      rev1++;//the remainder increases as the row increases
      rev2++;//the remainder increases as the row increases
      rev3--;//the remainder decreases as the row increases
      rev4--;//the remainder decreases as the row increases
    }//end of for loop
    System.out.println("\n");
    
    System.out.println("\nWhile Loop");
    rev1 = 1;//remainder for upper left section spaces
    rev2 = side;//remainder for upper right spaces
    rev3 = side;//remainder for lower left spaces
    rev4 = side - 1;//remainder for lower right spaces
    int x = 0;//variable for looping while-loop
    while(x < side){
      System.out.println("");//print new line
      int j = 1;//variable for looping
      while(j <= side){//print symbol 
         if(j % (2*side) == rev1){
           System.out.print(" ");//print out the space on the upper left section 
         }else if((j-1) % (2*side) == rev2){
           System.out.print (" ");//print out the space on the upper right section
         }else if(j % (2*side) == rev3){
           System.out.print(" ");//print out the space on the lower left section
         }else if((j-1) % side == rev4){
           System.out.print(" ");//print out the space on the lower right section 
         }else{
           System.out.print("*");//fill all the other with *
         }
        j++;//go to next symbol
      }
      x++;//go to next line
      
      rev1++;//the remainder increases as the row increases
      rev2++;//the remainder increases as the row increases
      rev3--;//the remainder decreases as the row increases
      rev4--;//the remainder decreases as the row increases
    }//end of while loop
    System.out.println("\n");
    
    System.out.println("\nDo-While Loop");
    rev1 = 1;//remainder for upper left section spaces
    rev2 = side;//remainder for upper right spaces
    rev3 = side;//remainder for lower left spaces
    rev4 = side - 1;//remainder for lower right spaces
    int y = 0;//variable for looping
    do{
      System.out.println("");//print new line
      int j = 1;//variable for looping 
      do{
        if(j % (2*side) == rev1){
           System.out.print(" ");//print out the space on the upper left section 
         }else if((j-1) % (2*side) == rev2){
           System.out.print (" ");//print out the space on the upper right section
         }else if(j % (2*side) == rev3){
           System.out.print(" ");//print out the space on the lower left section
         }else if((j-1) % side == rev4){
           System.out.print(" ");//print out the space on the lower right section 
         }else{
           System.out.print("*");//fill all the other with *
         }
        j++;
      }while(j <= side);
      y++;//go to next line
      
      rev1++;//the remainder increases as the row increases
      rev2++;//the remainder increases as the row increases
      rev3--;//the remainder decreases as the row increases
      rev4--;//the remainder decreases as the row increases
    }while(y < side);
    System.out.println("\n");
    
  }//end of main method
  
}//end of class
