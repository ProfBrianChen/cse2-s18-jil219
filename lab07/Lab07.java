//////////////////////////////
//CSE2 LAB07 JIAHUI LI
//Generates sentence randomly
/////////////////////////////

import java.util.Random;
import java.util.Scanner;

public class Lab07{//beginning of class
  
  public static void main(String[] args){//main method 
    Scanner myScanner = new Scanner(System.in);//declare scanner object
    int i = random();//generate random number and save as i
    String subject1 = sub(i);//invoke method to generate a subject  
    System.out.println("\nThe"+ adj(random())+ adj(random()) + subject1 + ver(random()) 
                       + "the" + adj(random()) + sub(random()) + ".\n");//print sentence 
    int check = 0;//for checking if user wants another sentence
    while(check != 1){//loop to generate another sentence 
      System.out.print("Would you like one more sentence?(yes/no) ");//prompt user for choice
      String userIn = myScanner.next();//accept user input 
      if(userIn.equals("yes")){
        System.out.println("The"+ adj(random())+ adj(random()) + sub(random()) + ver(random()) 
                       + "the" + adj(random()) + sub(random()) + ".\n");//print another sentence 
      }else if(userIn.equals("no")){
        check++;//end loop if user doesn't want another sentence
      }else{//when user input other things 
        System.out.println("Please enter 'yes' or 'no'.\n");
      }
    }
    extend(subject1);//invoke method for second sentence 
    conclusion(subject1);//invoke method for conclusion
  }//end of main method
  
  public static int random(){//the method to generate a random number
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    return randomInt;//save the random number
  }
  
  public static String adj(int rand1) {//the method to generate an adjective
    String adj = " ";//initialize the adjective
    switch(rand1){//list 10 cases
      case 0:
        adj = " beautiful ";
        break;
      case 1:
        adj = " red ";
        break;
      case 2:
        adj = " green ";
        break;
      case 3:
        adj = " big ";
        break;
      case 4:
        adj = " nice ";
        break;
      case 5:
        adj = " friendly ";
        break;
      case 6:
        adj = " elegant ";
        break;
      case 7:
        adj = " black ";
        break;
      case 8:
        adj = " arrogant ";
        break;
      case 9:
        adj = " mean ";        
        break;
    }  
    return adj;//save the adjective
  }
  
  public static String sub(int rand2) {//the method to generate a noun for the subject
    String sub = " ";//initialize the subject
    switch (rand2){//list 10 cases
      case 0:
        sub = " fox ";
        break;
      case 1:
        sub = " panda ";
        break;
      case 2:
        sub = " pig ";
        break;
      case 3:
        sub = " cat ";
        break;
      case 4:
        sub = " dog ";
        break;
      case 5:
        sub = " tiger ";
        break;
      case 6:
        sub = " lion ";
        break;
      case 7:
        sub = " bird ";
        break;
      case 8:
        sub = " mouse ";
        break;
      case 9:
        sub = " wolf ";
        break;
    }
    return sub;//save the subject
  }
  
  public static String ver(int rand3) {//the method to genenrate a verb
    String verb = " ";//initialize the verb
    switch (rand3){//list 10 cases
      case 0:
        verb = " played with ";
        break;
      case 1:
        verb = " talked to ";
        break;
      case 2:
        verb = " poked ";
        break;
      case 3:
        verb = " organized ";
        break;
      case 4:
        verb = " gathered ";
        break;
      case 5:
        verb = " caught ";
        break;
      case 6:
        verb = " looked at ";
        break;
      case 7:
        verb = " pushed ";
        break;
      case 8:
        verb = " touched ";
        break;
      case 9:
        verb = " slapped ";
        break;
    }
    return verb;//save the verb
  }
  
  public static String obj(int rand4) {//the method to generate a noun for the object
    String obj = " ";//initialize the object
    switch (rand4){//list all the 10 cases
      case 0:
        obj = " desk ";
        break;
      case 1:
        obj = " chair ";
        break;
      case 2:
        obj = " bowl ";
        break;
      case 3:
        obj = " plate ";
        break;
      case 4:
        obj = " bottle ";
        break;
      case 5:
        obj = " cup ";
        break;
      case 6:
        obj = " orange ";
        break;
      case 7:
        obj = " grass ";        
        break;
      case 8:
        obj = " flower ";
        break;
      case 9:
        obj = " ball ";
        break;
    }
    return obj;//save the object
  }
  
  public static void extend(String subject1){//method to generate the second phase
    Random randomGenerator = new Random();
    int i = randomGenerator.nextInt(10);//generate random number 
    if(i % 2 == 0){//when random number generated is even 
      System.out.println("This" + subject1 + "was" + adj(random()) + adj(random()) + "to" + adj(random()) + obj(random()) + "."); 
    }else{//when random number generated is odd
      System.out.println("It used" + obj(random()) + "to" + ver(random()) + obj(random()) + "with the" + adj(random()) + sub(random()));
    }
  }
  public static void conclusion(String subject1){//method to generate the conclusion
    System.out.println("That"+ subject1 + ver(random()) + "her" + obj(random()) + "!\n"); 
  }
}
