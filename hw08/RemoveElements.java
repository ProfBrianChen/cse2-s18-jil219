////////////////////////////////////
//CSE2 HW08 JIAHUI LI
//generate random array with 10 ints
//delete int at position chosen 
//remove ints chosen by user
////////////////////////////////////

import java.util.Scanner;
import java.util.Random;

public class RemoveElements{//class 

  public static void main(String [] arg){//main method 
    Scanner scan = new Scanner(System.in);
    int[] num = new int[10];//declare array to store 10 ints 
    int[] newArray1;//declare array for one position removed 
    int[] newArray2;//declare array for chosen ints removed
    int index,target;//declare ints for user inputs 
    String answer = " ";//user input whether go again or not 
    
    do{//output array and prompt user for input 
      System.out.print("Random input 10 ints[0-9] ");
      num = randomInput();//call method for generating random array 
      String out = "The original array is: ";
      out += listArray(num);//call method to change the array to String 
      System.out.println(out);//output array as a String
      
      System.out.print("Enter the index[0-9]: ");
      index = scan.nextInt();//prompt user for index and accept user input
      newArray1 = delete(num,index);//call method to generate new array 
      String out1 = "The output array is: ";
      out1 += listArray(newArray1);//call method to change new array to String
      System.out.println(out1);//output new array as a String
      
      System.out.print("Enter the target value: ");
      target = scan.nextInt();//prompt user for target value and accept user input
      newArray2 = remove(num,target);//call method to remove target value 
      String out2 = "The output array is ";
      out2 += listArray(newArray2);//call method to change new array to String
      System.out.println(out2);//output new array as a String
      
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit- ");
      answer = scan.next();//ask user whether to rerun or not
    }while(answer.equals("Y") || answer.equals("y"));
  }
  public static String listArray(int[] num){//method to change array to a String
    String out="{";
    for(int i = 0;i < num.length;i++){
    //loop to add array elements to the String 
      if(i>0){
        out+=", ";
      }
      out += num[i];
    }
    out += "}";
    return out;//return the String to main method 
  }
  public static int[] randomInput(){//method for generating random array
    Random rand = new Random();//declare random generator 
    int x[] = new int[10];//initialize array 
    for(int i=0;i<x.length;i++){
    //loop to generate 10 random numbers and add to array 
      x[i] = rand.nextInt(10);
    }
    return x;//return array to main method 
  }
  public static int[] delete(int[] list,int num){//method for deleting int at chosen position
    int[] x = new int[list.length - 1];//initialize new array with 9 elements
    if(num<0 || num>9){//when chosen position is out of range 
      System.out.println("The index is not valid.");
      return list;//the array is not changed 
    }
    int k = 0;//k indicates position in array x
    for(int i = 0;i < list.length;i++){
    //check elements in original array
      if(i != num){//for positions other than the chosen position
        x[k] = list[i];//store the element in the new array 
        k++;//go to next position in array x
      }
    }
    return x;//return new array
  }
  public static int[] remove(int[] list,int num){//method for removing chosen ints 
    int ct = 0;//count the number of chosen int in the original array 
    for(int i = 0;i < list.length;i++){//check each element in original array 
      if(list[i] == num){
        ct++;//count is incremented each time a chosen int is found 
      }
    }
    if(ct == 0){//when chosen element is not in the array
      System.out.println("Element "+ num +" was not found.");
      return list;//return the original array
    }else{
      System.out.println("Element "+ num +" was found.");
    }
    int[] x = new int[list.length - ct];//initialize new array which doesn't store the chosen ints 
    int k = 0;//k indicates position in array x
    for(int j = 0;j < list.length;j++){
    //check elements in the original array
      if(list[j] != num){
      //store the elements not equal to the chosen int in the new array
        x[k] = list[j];
        k++;//go to next position in array x
      }
    }
    return x;//return new array 
  }
  
}